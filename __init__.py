# python imports
import time

# flow3r imports
from st3m.application import Application, ApplicationContext
from ctx import Context
from st3m.input import InputController, InputState
from st3m import logging
import leds

log = logging.Log(__name__, level=logging.INFO)
log.info("1, 2, ... Disco!")

def hsv(h:float, s:float, v:float) -> tuple[float, float, float]:
    h = h / 360.0 % 1.0
    if s:
        i = int(h * 6.0)
        f = h * 6.0 - i
        
        w = v * (1.0 - s)
        q = v * (1.0 - s * f)
        t = v * (1.0 - s * (1.0 - f))
        
        if i==0: return (v, t, w)
        if i==1: return (q, v, w)
        if i==2: return (w, v, t)
        if i==3: return (w, q, v)
        if i==4: return (t, w, v)
        if i==5: return (v, w, q)
    else:
        return (v, v, v)

def avgdiff(a:list) -> int:
    l = len(a) - 1
    if l <= 0: return 0
    else: return int(sum([t - s for s, t in zip(a, a[1:])]) / l)

def clamp(x:float, minVal:float, maxVal:float) -> float:
    return min(max(x, minVal), maxVal)

def mix(x:float, y:float, a:float) -> float:
    return x * (1.0 - a) + y * a

def step(step:float, x:float) -> float:
    if x<step: return 0.0
    else: return 1.0

def smoothstep(edge0:float, edge1:float, x:float) -> float:
    t = clamp((x - edge0) / (edge1 - edge0), 0.0, 1.0)
    return t * t * (3.0 - 2.0 * t);

def getnow() -> int:
    return time.time_ns()

def timescale(seconds: float) -> int:
    return int(seconds*1e9)


class Animation:

    _BEAT_DEBOUNCE = timescale(0.2)

    def __init__(self) -> None:
        self._t0 = 0
        self._dt = 1

    def reset(self, t: int, delta: int) -> None:
        self._t0 = t
        self._dt = delta

    def beat(self, t: int, delta: int) -> None:
        time_since_t0 = t - self._t0
        beats_since_t0 = time_since_t0 // self._dt
        time_since_beat = time_since_t0 % self._dt

        if time_since_beat > self._BEAT_DEBOUNCE:  # Sync to next beat if current beat is 90% done.
            beats_since_t0 += 1

        self._t0 = t - beats_since_t0 * delta
        self._dt = delta

    def update(self, t: int) -> bool:
        """Update internal state for time t and return whether call to .render() is required"""
        pass

    def render(self) -> None:
        """Render animation according to internal state"""
        pass

class AllBlinkyAnim(Animation):

    _START_COLOR = [120.0, 1.0, 1.0]
    _FONT_ID = 5
    _FONT_SIZE = 115
    _LOGO_TEXT = "d1sco"

    def __init__(self) -> None:
        super().__init__()
        self._display_hue = 0.0

    def update(self, t:int) -> bool:
        time_since_t0 = t - self._t0
        beats_since_t0 = time_since_t0 // self._dt
        hue_to_render = beats_since_t0 % 6 * 60.0
        if hue_to_render != self._display_hue:
            self._display_hue = hue_to_render
            return True
        else:
            return False

    def render(self, ctx:Context) -> None:
        _hsv = list(self._START_COLOR)
        _hsv[0] += self._display_hue
        _rgb = hsv(*_hsv)

        ctx.save()

        ctx.rgb(*_rgb).rectangle(-120, -120, 240, 240).fill()
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.font = ctx.get_font_name(self._FONT_ID)
        ctx.font_size = self._FONT_SIZE
        ctx.rgb(0, 0, 0)
        ctx.move_to(0, -7)
        ctx.text(self._LOGO_TEXT)

        ctx.font = ctx.get_font_name(0)
        ctx.font_size = 15
        ctx.move_to(0, 40)
        ctx.text(f"{60.0 / (self._dt / 1e9):.1f} bpm")

        ctx.restore()

        leds.set_all_rgb(*_rgb)
        leds.update()

class DiscoApp(Application):

    _PAUSE_DELAY = timescale(10.0)
    _TAP_TIMEOUT = 1
    _TAP_AVG_BUFFER = 32 + 1
    _BRIGHT_STEP = 0.1

    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        now = getnow()
        self._beat_delay = timescale(1.0)
        self._touch_state = False
        self._taps = [now - self._TAP_TIMEOUT]
        self._brightness = 100
        self.input = InputController()

        leds.set_auto_update(False)
        leds.set_slew_rate(255)
        leds.set_brightness(self._brightness)
        leds.set_all_rgb(0, 0, 0)
        leds.update()

        self.anim = AllBlinkyAnim()
        self.anim.reset(now, self._beat_delay)

    def on_exit(self):  # Never called :()
        log.info("Disco: Closing time! You don't have to go home, but you can't stay here.")
        leds.set_auto_update(False)
        leds.set_all_rgb(0, 0, 0)
        leds.set_brightness(69)
        leds.set_slew_rate(255)
        leds.update()
        super().on_exit()

    def draw(self, ctx: Context) -> None:
        # Must always render, else glitch
        now = getnow()
        self.anim.update(now)
        self.anim.render(ctx)
        # log.info("Disco: beat")


    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)

        bright_step = None
        if self.input.buttons.app.left.pressed or self.input.buttons.app.left.repeated:
            bright_step = -max(2, int(leds.get_brightness() * self._BRIGHT_STEP))
        elif self.input.buttons.app.right.pressed or self.input.buttons.app.right.repeated:
            bright_step = max(2, int(leds.get_brightness() * self._BRIGHT_STEP))
        elif self.input.buttons.app.middle.pressed:
            if leds.get_brightness() > 0:
                leds.set_brightness(0)
            else:
                leds.set_brightness(self._brightness)
            leds.update()

        if bright_step is not None:
            self._brightness = clamp(self._brightness + bright_step, 0, 255)
            log.info("Disco: brightness " + str(self._brightness))
            leds.set_brightness(self._brightness)
            leds.update()

        is_touched = False
        for p in range(0, 10, 2):
            if self.input.captouch.petals[p].pressure > 0:
                is_touched = True

        if is_touched:
            self.touched()
        else:
            self.released()

    def touched(self) -> None:
        if not self._touch_state:
            # log.info("Disco: touched")
            self._touch_state = True
            self.tapped()

    def released(self) -> None:
        if self._touch_state:
            # log.info("Disco: released")
            self._touch_state = False

    def tapped(self) -> None:
        log.info("Disco: tap")
        now = getnow()
        if now - self._taps[-1] >= 1.3 * self._TAP_TIMEOUT * self._beat_delay :
            self._taps = [now]
            self._beat_delay = self._PAUSE_DELAY
            self.anim.reset(now, self._beat_delay)
            return

        self._taps.append(now)
        if len(self._taps) > self._TAP_AVG_BUFFER:
            self._taps = self._taps[-self._TAP_AVG_BUFFER:]

        if len(self._taps) >= 2:
            self._beat_delay = avgdiff(self._taps)

        self.anim.beat(now, self._beat_delay)


# For running with `mpremote run`:
if __name__ == "__main__":
    import st3m.run

    st3m.run.run_view(DiscoApp(ApplicationContext()))
