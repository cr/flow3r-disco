.PHONY: run
run:
	mpremote run __init__.py

.PHONY: install
install:
	-mpremote mkdir :/flash/sys/apps/cr-flow3r-disco
	mpremote cp __init__.py flow3r.toml :/flash/sys/apps/cr-flow3r-disco/
	-mpremote reset &>/dev/null

.PHONY: remove
remove:
	-mpremote rm :/flash/sys/apps/cr-flow3r-disco/flow3r.toml :/flash/sys/apps/cr-flow3r-disco/__init__.py :/flash/sys/apps/cr-flow3r-disco/
	-mpremote reset &>/dev/null

.PHONY: siminstall
siminstall:
	-mkdir ../flow3r-firmware/python_payload/apps/cr-flow3r-disco
	cp __init__.py flow3r.toml ../flow3r-firmware/python_payload/apps/cr-flow3r-disco/

.PHONY: simremove
simremove:
	-rm ../flow3r-firmware/python_payload/apps/cr-flow3r-disco/*
	-rmdir ../flow3r-firmware/python_payload/apps/cr-flow3r-disco/

.PHONY: sim
simrun: siminstall
	cd ../flow3r-firmware && python sim/run.py
