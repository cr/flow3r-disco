# flow3r-disco v2

Your personal disco light show. Show appreciation for the music around you by tapping any rose petal to its beat, and flow3r-disco will sync to the rhythm.

## Prerequisites
* Developed on flow3r-firmware v1.2.0
* Active python environment has `mpremote` in path

## Usage
* Test run with `make run`.
* Install with `make install`.
* Tap any rose petal in rhythm to sync beat changes.
* Use left app wheel to decrease/increase LED brightness.
* Tap averaging is reset after five seconds.
* Make a single tap to "pause" (actually slowed to 6 BPM).
* Uninstall with `make remove`.

## Known issues
* Simulator can't run it for lack of various `leds` methods.
* LEDs won't switch off on app exit.
  * Workaround: tap left wheel to dim LEDs prior to exit.
* BPM timing is subject to excessive drift due to `think()` callback granularity.
* Makefile throws a bunch of benign errors. Please try to ignore.
